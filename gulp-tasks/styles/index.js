const	gulp = require('gulp'),
		sass = require('gulp-sass'),
		px2rem = require('gulp-px-to-rem'),
		autoprefixer = require('gulp-autoprefixer'),
		cleanCss = require('gulp-clean-css');

module.exports = function() {
	gulp.task('sass', ['styles:lint'], () => {
		return gulp.src('style/style.scss')
			.pipe(sass())
			.pipe(px2rem({rootPX:10}))
			.pipe(autoprefixer({
				browsers: ['last 10 versions', 'ie 10-11'],
				cascade: false
			}))
			.pipe(gulp.dest('htdocs/assets/css'))
			.pipe(cleanCss({
				compatibility: 'ie10',
				inline: ['local']
			}))
			.pipe(gulp.dest('htdocs/assets/css'));
	});

	return gulp.start('sass');
};