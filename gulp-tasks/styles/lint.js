const	gulp = require('gulp'),
		sassLint = require('gulp-sass-lint');

module.exports = function() {
	return gulp.src('style/**/*.scss')
			.pipe(sassLint({
				configFile: 'config-sass-lint.yml'
			}))
			.pipe(sassLint.format())
			.pipe(sassLint.failOnError());
};