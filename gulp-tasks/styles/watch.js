const	gulp = require('gulp'),
		watch = require('gulp-watch');

module.exports = function() {

	gulp.task('watchStyles', function() {
		gulp.start('styles:lint');
		
		watch('style/**/*.scss', {usePolling: true}, function() {
			gulp.start('styles:lint');
			gulp.start('styles:dev');
		});
	});

	return gulp.start('watchStyles');
};