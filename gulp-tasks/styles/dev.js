const	gulp = require('gulp'),
		sass = require('gulp-sass'),
		px2rem = require('gulp-px-to-rem'),
		autoprefixer = require('gulp-autoprefixer'),
		plumber = require("gulp-plumber"),
		livereload = require('gulp-livereload');

module.exports = function() {
	return gulp.src('style/style.scss')
			.pipe(plumber({
				errorHandler: function (err) {
					console.log(err);
					this.emit('end');
				}
			}))
			.pipe(sass())
			.pipe(px2rem({rootPX:10}))
			.pipe(autoprefixer({
				browsers: ['last 10 versions', 'ie 10-11'],
				cascade: false
			}))
			.pipe(gulp.dest('htdocs/assets/css'))
			.pipe(livereload());
};