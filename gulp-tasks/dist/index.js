const	gulp = require('gulp'),
		dist = require('gulp-npm-dist');

module.exports = function() {
	return gulp.src(dist(), {base:'./node_modules'})
		.pipe(gulp.dest('./htdocs/vendor'));
};