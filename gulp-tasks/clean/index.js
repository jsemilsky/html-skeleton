const	gulp = require('gulp'),
		del = require('del');

module.exports = function() {
	return del([
		'htdocs/assets/css',
		'htdocs/assets/js',
		'htdocs/assets/fonts',
		'htdocs/assets/images',
		'htdocs/vendor',
		'htdocs/*.html'
	]);
};