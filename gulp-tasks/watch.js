const	gulp = require('gulp'),
		watch = require('gulp-watch'),
		livereload = require('gulp-livereload');

module.exports = function() {

	// build dev styles.json
	gulp.start('styles:dev');

	// build dev scripts.json
	gulp.start('scripts:dev');
	
	//Watch styles
	gulp.start('styles:watch');

	// Watch scripts files
	watch('js/**/*', {usePolling: true}, function() {
		gulp.start('scripts:dev');
	});

	// Watch scripts json
	watch('js/scripts.json', {usePolling: true}, function() {
		gulp.start('scripts:dev');
	});

	// Watch image files
	watch('images/**/*', {usePolling: true}, function() {
		gulp.start('images');
	});

	// Watch latte files
	watch('templates/**/*.html', {usePolling: true}, function() {
		gulp.start('html:watch');
		livereload.reload();
	});

	// Create LiveReload server
	livereload.listen();
};