const	gulp = require('gulp'),
		concat = require('gulp-concat'),
		fs = require('fs'),
		livereload = require('gulp-livereload');

module.exports = function() {

	var jsFiles = JSON
			.parse(fs.readFileSync('js/scripts.json'))
			.map(function (item) {
				return item = 'js/' + item;
			});

	return gulp.src(jsFiles)
				.pipe(concat('scripts.js'))
				.pipe(gulp.dest('htdocs/assets/js'))
				.pipe(livereload());
};