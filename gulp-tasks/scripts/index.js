const	gulp = require('gulp'),
		uglify = require('gulp-uglify'),
		concat = require('gulp-concat'),
		fs = require('fs');

module.exports = function() {

	var jsFiles = JSON
			.parse(fs.readFileSync('js/scripts.json'))
			.map(function (item) {
				return item = 'js/' + item;
			});

	return gulp.src(jsFiles)
				.pipe(concat('scripts.js'))
				.pipe(uglify())
				.pipe(gulp.dest('htdocs/assets/js'));
};