const	gulp = require('gulp');

module.exports = function() {
	gulp.task('default', ['clean'], function() {
		gulp.start('dist', 'html', 'styles', 'scripts', 'fonts', 'images');
	});

	return gulp.start('default');
};