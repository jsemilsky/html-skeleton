const 	gulp = require('gulp'),
		changed = require('gulp-changed'),
		livereload = require('gulp-livereload');

module.exports = function() {
	return gulp.src('images/**/*')
			.pipe(changed('htdocs/assets/images'))
			.pipe(gulp.dest('htdocs/assets/images'))
			.pipe(livereload());
};