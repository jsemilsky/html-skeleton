const	gulp = require('gulp'),
		changed = require('gulp-changed'),
		cleanCss = require('gulp-clean-css'),
		concat = require('gulp-concat');

module.exports = function() {
	gulp.task('fontsFiles', function() {
		return gulp.src('fonts/**/*.{otf,ttf,woff,woff2,svg}')
				.pipe(changed('htdocs/assets/fonts'))
				.pipe(gulp.dest('htdocs/assets/fonts'));
	});

	gulp.task('fonts', ['fontsFiles'], function () {
		return gulp.src('fonts/**/*.css')
				.pipe(concat('fonts.css'))
				.pipe(cleanCss({compatibility: 'ie10'}))
				.pipe(gulp.dest('htdocs/assets/fonts'));
	});

	return gulp.start('fonts');
};