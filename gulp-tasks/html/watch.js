const	gulp = require('gulp'),
		changed = require('gulp-changed'),
		fileinclude = require('gulp-file-include'),
		livereload = require('gulp-livereload');

module.exports = function() {
	return gulp.src(['templates/*.html'])
			.pipe(fileinclude({
				prefix: '@@',
				basepath: '@file'
			}))
			.pipe(gulp.dest('./htdocs/'))
			.pipe(livereload());
};

