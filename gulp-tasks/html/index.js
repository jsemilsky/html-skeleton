const	gulp = require('gulp'),
		fileinclude = require('gulp-file-include');

module.exports = function() {
	return gulp.src(['templates/*.html'])
			.pipe(fileinclude({
				prefix: '@@',
				basepath: '@file'
			}))
			.pipe(gulp.dest('./htdocs/'));
};