/* js/o-header.js */

$(document).ready(function() {
    $.mq.action('menu', function() {
        $('.jq_menu_status').prop('checked', false).trigger('change');
    }, function() {
        $('.jq_menu_status').prop('checked', false).trigger('change');
    });

    $('.jq_menu_status, .jq_search_status').change(function() {
        if (this.checked) {
            if ($(this).hasClass('jq_search_status') && $('.jq_menu_status').is(':checked')) {
                $('.jq_menu_status').prop('disabled', true);
            } else {
                $('html, body').addClass('no-scroll');
            }

            $(document).on('keyup', {status: this}, closeOnEscape);
        } else {
            if ($(this).hasClass('jq_search_status') && $('.jq_menu_status').is(':disabled')) {
                $('.jq_menu_status').prop('disabled', false);
            } else {
                $('html, body').removeClass('no-scroll');
            }

            $(document).off('keyup', closeOnEscape);
        }
    });

    function closeOnEscape(event) {
        if (event.keyCode == 27) {
            var args = event.data,
                status = args.status;
            $(status).prop('checked', false).trigger('change');
        }
    }
});