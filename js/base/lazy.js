var Lazy = function() {
    var test = function() {
        return $('.lazyload, img[data-src], img[data-srcset], [data-bg], [data-bgset]').length ? true : false;
    };

    var init = function() {
        if (test()) {
            document.addEventListener('lazybeforeunveil', function(e) {
                $(e.target).filter('[data-bg]').css('background-image', 'url(' + $(e.target).data('bg') + ')');
            });

            if (typeof window.lazySizesConfig !== 'undefined' && typeof $.mq !== 'undefined') {
                var customMedia = {};

                $.each($.mq.getBreakpoints(), function(rule, queries) {
                    var mqIndex = queries.indexOf(' and ');

                    if (mqIndex > 0) {
                        customMedia['-' + rule + '-'] = queries;
                        customMedia['--' + rule] = queries.slice(0, mqIndex);
                        customMedia[rule + '--'] = queries.slice(mqIndex + 5);
                    } else if (queries.indexOf('max-width') > 0) {
                        customMedia[rule + '--'] = queries;
                    } else {
                        customMedia['--' + rule] = queries;
                    }
                });

                if (!$.isEmptyObject(customMedia)) window.lazySizesConfig.customMedia = customMedia;

                window.lazySizes.init();
            }
        }
    };

    return {
        init: init
    };
}();

$(document).ready(Lazy.init);