var AutoFocus = function() {
    var dataAttr = 'auto-focus';
    
    var test = function() {
        return $('[data-' + dataAttr + ']').length ? true : false;
    };

    var init = function() {
        if (test()) {
            $('[data-' + dataAttr + ']').click(function() {
                var targetSelector = $(this).data(dataAttr);

                setTimeout(function() {
                    $(targetSelector).focus();
                }, 10);
            });
        }
    };

    return {
        init: init
    };
}();

$(document).ready(AutoFocus.init);