var EqualRow = function() {
    var test = function() {
        return typeof $.mq !== 'undefined' ? true : false;
    }

    var init = function() {
        if (test()) {
            $('.jq_equal_row').each(function() {
                var $el = {
                        row: $(this),
                        columns: $('[data-equal-group]', $(this))
                    };
                var groups = {};
                var breakpoint = $el.row.attr('data-breakpoint') || 'xl';

                $.mq.action(breakpoint, function() {
                    $el.columns.css('height', '').each(function() {
                        var columnHeight = $(this).height(),
                            columnGroup = $(this).attr('data-equal-group');

                        if (typeof groups[columnGroup] === 'undefined') groups[columnGroup] = 0;

                        if (columnHeight > groups[columnGroup]) groups[columnGroup] = columnHeight;
                    });

                    $.each(groups, function(group, height) {
                        if (height) $el.columns.filter('[data-equal-group="' + group + '"]').height(height);
                    });
                }, function() {
                    $el.columns.css('height', '');
                });
            });
        }
    };

    return {
        init: init
    };
}();

$(document).ready(EqualRow.init);