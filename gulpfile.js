/*!
 * Gulp + HTML
 * 
 * @author Jan Semilský
 * @version 0.0.1
 * 
 * Install:
 * install node.js
 * npm install gulp -g
 * npm install
 * install "LiveReload" plugin to Google Chrome, https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en
 * 
 * Gulp tasks:
 * All gulp tasks are specified in folder gulp-tasks and they are loaded automatically
 * 
 */

// Load plugins
var gulpRequireTasks = require('gulp-require-tasks')();